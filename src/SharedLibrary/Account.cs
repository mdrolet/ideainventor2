﻿namespace SharedLibrary
{
    /// <summary>
    /// Represents a User Account.
    /// </summary>
    public class Account
    {
        /// <summary>
        /// Gets or Sets the AccountID used internally by the Database.
        /// </summary>
        public int AccountID { get; set; }

        /// <summary>
        /// Gets or Sets the User Name of the account.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or Sets the Hashed Password of the account.
        /// </summary>
        public string HashedPassword { get; set; }

        /// <summary>
        /// Gets or Sets the account roles.
        /// </summary>
        //public Role Roles { get; set; }
    }
}