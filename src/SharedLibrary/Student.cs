﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedLibrary
{
    public class Student
    {
        public int? Age { get; set; }
        public bool? IsRegularStudent { get; set; }
        public string Name { get; set; }
        public Guid Id { get; set; }

    }
}
