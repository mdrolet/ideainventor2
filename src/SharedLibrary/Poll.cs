﻿namespace SharedLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Represents a Poll on the Forum.
    /// </summary>
    public class Poll
    {
        /// <summary>
        /// Gets or sets the title of the forum post.
        /// </summary>
        public string Title { get; set; }
    }
}
