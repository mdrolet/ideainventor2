﻿namespace Api
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using SharedLibrary;

    /// <summary>
    /// Entity Framework Context For Forum Posts.
    /// </summary>
    public class ForumPostContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ForumPostContext"/> class.
        /// </summary>
        /// <param name="options">Connection for the DB.</param>
        public ForumPostContext(DbContextOptions<ForumPostContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Account>()
            //    .ToTable("Account");
            //modelBuilder.Entity<Account>()
            //    .HasData(new Account
            //    {
            //        AccountID = 1,
            //        UserName = "test",
            //        HashedPassword = "test",
            //        //Roles = Role.Administrator
            //    });
            modelBuilder.Entity<Student>()
                .ToTable("Student");
            modelBuilder.Entity<Student>()
                .Property(s => s.Age)
                .IsRequired(false);
            modelBuilder.Entity<Student>()
                .Property(s => s.IsRegularStudent)
                .HasDefaultValue(true);

            modelBuilder.Entity<Student>()
                .HasData(
                    new Student
                    {
                        Id = Guid.NewGuid(),
                        Name = "John Doe",
                        Age = 30
                    },
                    new Student
                    {
                        Id = Guid.NewGuid(),
                        Name = "Jane Doe",
                        Age = 25
                    }
                );
        }

        /// <summary>
        ///  Gets or sets the DbSet associated with the Accounts.
        /// </summary>
        public DbSet<SharedLibrary.Student> Students { get; set; }

        /// <summary>
        ///  Gets or sets the DbSet associated with the Departments.
        /// </summary>
        //public DbSet<SharedLibrary.Department> Departments { get; set; }

        ///// <summary>
        /////  Gets or sets the DbSet associated with the Forums.
        ///// </summary>
        //public DbSet<SharedLibrary.Forum> Forums { get; set; }

        ///// <summary>
        /////  Gets or sets the DbSet associated with the Posts.
        ///// </summary>
        //public DbSet<SharedLibrary.ForumPost> Posts { get; set; }

        ///// <summary>
        /////  Gets or sets the DbSet associated with the Threads.
        ///// </summary>
        //public DbSet<SharedLibrary.ForumThread> Threads { get; set; }

        ///// <summary>
        /////  Gets or sets the DbSet associated with the Subscriptions.
        ///// </summary>
        //public DbSet<SharedLibrary.Subscription> Subscriptions { get; set; }

        ///// <summary>
        /////  Gets or sets the DbSet associated with the Tags.
        ///// </summary>
        //public DbSet<SharedLibrary.Tag> Tags { get; set; }
    }
}
