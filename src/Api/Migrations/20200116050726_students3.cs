﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Api.Migrations
{
    public partial class students3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: new Guid("6c49f815-7555-4e67-94a1-c892aa13f71b"));

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: new Guid("ec40442e-3cca-4365-99c4-bfcf3a6fe189"));

            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "Id", "Age", "Name" },
                values: new object[] { new Guid("2a9845b5-71eb-4eb8-ac5f-47c368b5367d"), 30, "John Doe" });

            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "Id", "Age", "Name" },
                values: new object[] { new Guid("fd1a6cf1-992e-41f5-bb8d-76e4fa31f1ab"), 25, "Jane Doe" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: new Guid("2a9845b5-71eb-4eb8-ac5f-47c368b5367d"));

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: new Guid("fd1a6cf1-992e-41f5-bb8d-76e4fa31f1ab"));

            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "Id", "Age", "Name" },
                values: new object[] { new Guid("ec40442e-3cca-4365-99c4-bfcf3a6fe189"), 30, "John Doe" });

            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "Id", "Age", "Name" },
                values: new object[] { new Guid("6c49f815-7555-4e67-94a1-c892aa13f71b"), 25, "Jane Doe" });
        }
    }
}
