﻿namespace Api.Configurations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Configuration Class for ForumPostController.
    /// </summary>
    public class ForumPostConfiguration
    {
        /// <summary>
        /// Gets or sets The Connection String for the DB Context.
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
