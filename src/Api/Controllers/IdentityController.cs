﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Api.Configurations;
using System;

namespace Api.Controllers
{
    [Route("identity")]
    [Authorize]
    public class IdentityController : ControllerBase
    {
        private ForumPostContext forumDb;
        private readonly ForumPostConfiguration forumConfig;
        private ILogger<IdentityController> logger;

        public IdentityController(ILogger<IdentityController> logger, IConfiguration config, ForumPostContext forumDb)
        {
            this.logger = logger;
            config.Bind("Forum", this.forumConfig);
            this.forumDb = forumDb;
        }

        public IActionResult Get()
        {
            return  new JsonResult(from c in forumDb.Students select new { c.Name, c.Age });
            //return new JsonResult(from c in User.Claims select new { c.Type, c.Value });
        }
    }
}